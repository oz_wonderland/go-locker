import QtQuick 2.13
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.11


Rectangle {
    property int widthBar
    property string iconCirclePath
    property string barText

    width: widthBar
    height: 93
    radius: 20
    clip: false
    color: "#2C7873"
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.top: parent.top
    anchors.leftMargin: 23
    anchors.rightMargin: 23
    anchors.topMargin: 35


    Text {
        id: bar_text
        text:  barText
        font.weight: Font.Bold
        font.pointSize: 48
        font.letterSpacing: 1.1
        font.family: "Open Sans"
        color: "#ffffff"
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: topBar_circle.right
        anchors.leftMargin: 15
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

    }

    Image {
        id: logo_image_bar
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        source: "drawables/vetgoLogo.png"
        anchors.rightMargin: 15
    }

    Rectangle {
         id: topBar_circle
         width: 60
         height: 60
         anchors.verticalCenter: parent.verticalCenter
         anchors.left: parent.left
         anchors.leftMargin: 25
         radius: width*0.5
         color: "#6FB98F"

         Image {
             id: icon_circle_topbar
             anchors.verticalCenter: parent.verticalCenter
             source: iconCirclePath
             anchors.horizontalCenter: parent.horizontalCenter

         }
    }
}
