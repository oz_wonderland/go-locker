import QtQuick 2.12
import QtQuick.Layouts 1.11
import QtQuick.VirtualKeyboard 2.4
import "ComponentCreator.js" as ComponentCreator

Rectangle {
    id: keyboard_content_rectangle
    color: Styles.backgroundColor
    signal onKeyPressed(int index, string key)
    property var codeArray: []

    RowLayout {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: keyboard_screen_input_panel.top
        spacing: 5

        Repeater{
            id: code_input_repeater
            model: 6
            CodeInput {
                itemNumber: index
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
        }
    }

    InputPanel {
        id: keyboard_screen_input_panel
        y: parent.height - keyboard_screen_input_panel.height
        width: Styles.width
        anchors.left: parent.left
        anchors.right: parent.right
        active: false
    }
    Component.onCompleted: {
        code_input_repeater.itemAt(0).focus = true
        keyboard_screen_input_panel.visible = true
        keyboard_content_rectangle.onKeyPressed.connect(keyPressed)
    }
    Component.onDestruction: {
        keyboard_content_rectangle.onKeyPressed.disconnect(keyPressed)
    }

    function keyPressed(index, key) {
        codeArray[index] = key
        if (codeArray.length !== 6) {
            return
        }
        var userType = codeBridge.codeInput(codeArray.join(''))
        if (userType === "") {
            ComponentCreator.createMessagePopup(main_window, "Code inconnu").open()
            return
        }
        main_window.onOpenSettingsView()
        keyboard_screen_input_panel.visible = false
        codeArray = []
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:650;width:1280}
}
##^##*/

