import QtQuick 2.13
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.11


RoundButton{
    property int width_button
    property int height_button
    property string buttonText
    width: 345

    id: icon_button
    text:  qsTr("Retour")
    font.weight: Font.Bold
    font.pointSize: 24
    font.family: "Open Sans"

    contentItem: Text {
            font: icon_button.font
            color: "#ffffff"
            text: buttonText
            anchors.rightMargin: 7
            anchors.leftMargin: 3
            width: width_button
            wrapMode: Text.Wrap
            anchors.verticalCenter: parent.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        implicitWidth: width_button
        implicitHeight: height_button
        opacity: enabled ? 1 : 0.3
        color: icon_button.down ? "#3E9F99" : "#004445"
        border.width: 0
        radius: 10
    }
}
