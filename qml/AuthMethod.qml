import QtQuick 2.13

Rectangle {
    property string title
    property string imageSource
    signal clicked

    id: auth_method_rectangle
    height: 570
    color: Styles.backgroundColor

    Column {
        id: column
        anchors.fill: parent
        padding: 0
        spacing: 75
        anchors.bottomMargin: 150
        anchors.topMargin: 150

        Image {
            id: auth_method_image
            width: 200
            height: 200
            source: imageSource
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit

            MouseArea{
                anchors.fill: parent
                onClicked: auth_method_rectangle.clicked()
            }

        }

        Text {
            id: auth_method_text
            color: "white"
            text: qsTr(title)
            font.pixelSize: 50
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
            anchors.horizontalCenter: parent.horizontalCenter
            font.bold: true
            anchors.topMargin: 100


            MouseArea{
                anchors.fill: parent
                onClicked: auth_method_rectangle.clicked()
            }
        }
    }
}


