import QtQuick 2.13
import QtQuick.Controls 2.13

FocusScope{
    width: code_input_rectangle.width
    height: code_input_rectangle.height
    x: code_input_rectangle.x
    y: code_input_rectangle.y
    property int itemNumber

    Rectangle {
        id: code_input_rectangle
        color: "white"
        width: 100
        height: 100
        radius: 10

        TextField {
            id: code_input_text_field
            opacity: 1
            anchors.fill: parent
            focus: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            placeholderText: "*"
            leftPadding: 0
            anchors.margins: 5
            font.pointSize: 55
            maximumLength: 1
            font.underline: false
            font.bold: false
            font.hintingPreference: Font.PreferDefaultHinting
            font.capitalization: Font.AllUppercase
            cursorVisible: false
            Keys.onPressed: {
                nextItemInFocusChain().forceActiveFocus()
                keyboard_content_rectangle.onKeyPressed(itemNumber, event.text)
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;height:100;width:100}
}
##^##*/

