import QtQuick 2.13
import QtQuick.Controls 2.13

Popup {
    property string message
    property string iconPath
       id: popup
       modal: true
       focus: true
       closePolicy: Popup.CloseOnPressOutside | Popup.CloseOnEscape
       anchors.centerIn: Overlay.overlay
       opacity: 1
       Rectangle{
           id: popup_icon_rectangle
           width:1162
           height: 227
           radius: 10
           anchors.verticalCenter: parent.verticalCenter
           anchors.horizontalCenter: parent.horizontalCenter
           color: "#3E9F99"
           Text {
               id: popup_icon_message
               text: message
               anchors.verticalCenter: parent.verticalCenter
               anchors.left: pop_up_icon.right
               anchors.right: parent.right
               horizontalAlignment: Text.AlignHCenter
               verticalAlignment: Text.AlignVCenter
               wrapMode: Text.Wrap
               anchors.rightMargin: 50
               anchors.leftMargin: 50
               font.weight: Font.Bold
               font.family: "Open Sans"
               font.pointSize: 40
               color: "#FFFFFF"
           }
           Image {
               id: pop_up_icon
               anchors.verticalCenter: parent.verticalCenter
               anchors.left: parent.left
               source: iconPath
               sourceSize.height: 100
               sourceSize.width: 100
               anchors.leftMargin: 50
           }
       }

}

/*##^##
Designer {
    D{i:0;autoSize:true;formeditorZoom:0.6600000262260437;height:480;width:640}
}
##^##*/
