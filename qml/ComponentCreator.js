function loadComponent(qml) {
    var component = Qt.createComponent(qml)
    if (component.status != Component.Ready &&
        component.status == Component.Error) {
        console.debug("Error loading component (" + qml + "): " + component.errorString())
        return null
    }

    return component;
}

function createMessagePopup(parent, message) {
    var component = loadComponent("PopUpMessage.qml")
    if (component == null) {
        return null
    }

    var popup = component.createObject(parent, {
        "message": message
    })
    if (popup == null) {
        console.log("Error creating MessagePopup")
    }
    return popup
}