import QtQuick 2.13
import QtQuick.Layouts 1.11

Rectangle {
    id: user_management_content_rectangle
    color: Styles.backgroundColor

    //Sub bar
    SubBar {
        id: sub_bar_admin_management
        barText: "ADMIN'S"
    }
    //Icon Buttons bellow the subbar
    //New admin
    IconButton {
        id: new_admin_button
        anchors.top: sub_bar_admin_management.bottom
        anchors.topMargin: 38
        anchors.left: parent.left
        anchors.leftMargin: 23
        width_button: 448
        height_button: 56
        iconSource: "drawables/person_plus_fill.svg"
        buttonText: "Ajouter nouveau"
    }
    //Edit admin
    IconButton {
        id: edit_admin_button
        anchors.top: new_admin_button.top
        anchors.right: delete_admin_button.left
        anchors.rightMargin: 23
        width_button: 247
        height_button: 56
        iconSource: "drawables/pencil.svg"
        buttonText: "Modifier"
        onClicked: {
            stack_view.push(edit_admin)
        }
    }
    //Delete admin
    IconButton {
        id: delete_admin_button
        anchors.top: new_admin_button.top
        anchors.right: parent.right
        anchors.rightMargin: 23
        width_button: 247
        height_button: 56
        standardColorUp: "#D86363"
        standardColorDown: "#FF5F5F"
        iconSource: "drawables/delete.svg"
        buttonText: "Supprimer"
        onClicked: {
            popup_remove_admin.open()
        }
    }
    //List for profiles
    ProfileList {
        id: admin_profile_list
        title_value: title_value
        text_value: text_value
        anchors.top: new_admin_button.bottom
        anchors.topMargin: 38
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:650;width:1280}
}
##^##*/

