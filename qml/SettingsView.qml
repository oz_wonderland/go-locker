import QtQuick 2.13
import QtQuick.Controls 2.13

Page {
    id: settings_view_page

    Banner {
        id: settings_view_banner
        height: Styles.bannerHeight
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        title: "Paramètres"

        BackButton {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.rightMargin: 35
            anchors.bottomMargin: 40
            anchors.topMargin: 53
        }
    }
    SettingsContent{
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: settings_view_banner.bottom
    }
}
