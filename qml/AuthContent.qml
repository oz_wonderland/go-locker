import QtQuick 2.13
import QtQuick.Layouts 1.11

Rectangle {
    id: auth_content_rectangle

    RowLayout {
        anchors.fill: parent
        AuthMethod {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            width: Styles.width/2
            imageSource: "drawables/qr.svg"
            title: "Scanner le QR Code"
            onClicked: main_window.showQrCodeView()
        }
        AuthMethod {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            width: Styles.width/2
            imageSource: "drawables/keyboard.svg"
            title: "Introduire le code d'accès"
            onClicked: main_window.showKeyboardView()
        }
    }
}
