import QtQuick 2.13
import QtQuick.Layouts 1.11

Rectangle {
    id: profile_management_content_rectangle
    color: Styles.backgroundColor

    RowLayout{
        anchors.fill: parent
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        TextButton{
            id: profile_management_admin_button
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.row: 2
            Layout.column: 1
            width_button: 345
            height_button: 140
            Layout.preferredWidth: 345
            Layout.preferredHeight: 140
            buttonText: "Administrateur"
            Layout.rightMargin: 20
            onClicked: main_window.onOpenUserManagementView()
        }
        TextButton{
            id: profile_management_asv_button
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.row: 2
            Layout.column: 1
            width_button: 345
            height_button: 140
            Layout.preferredWidth: 345
            Layout.preferredHeight: 140
            buttonText: "ASV"
            onClicked: main_window.onOpenUserManagementView()
        }
     }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:650;width:1280}
}
##^##*/
