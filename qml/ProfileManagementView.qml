import QtQuick 2.0
import QtQuick.Controls 2.13

Page {
    id: profile_management_view_page

    Banner {
        id: profile_management_view_banner
        height: Styles.bannerHeight
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        title: "Gestion des profils"

        BackButton {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.rightMargin: 35
            anchors.bottomMargin: 40
            anchors.topMargin: 53
        }
    }
    ProfileManagementContent {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: profile_management_view_banner.bottom
    }
}
