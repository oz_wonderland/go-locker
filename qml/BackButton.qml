import QtQuick 2.13
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.11


RoundButton{
    id: back_button
    text:  qsTr("Retour")
    font.weight: Font.Bold
    font.pointSize: 24
    font.family: "Open Sans"
    onClicked: main_window.backClicked()

    Image {
        antialiasing: true
        id: image_back_button
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.alignWhenCentered: Text
        source: "drawables/arrow_return_left.svg"
        anchors.leftMargin: 7
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        fillMode: Image.PreserveAspectFit
    }

    contentItem: Text {
            font: back_button.font
            color: "#ffffff"
            text: "Retour"
            anchors.rightMargin: 8
            anchors.leftMargin: 5
            elide: Text.ElideRight
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: image_back_button.right
            anchors.right: parent.right
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        implicitWidth: 145
        implicitHeight: 63
        opacity: enabled ? 1 : 0.3
        color: back_button.down ? "#3E9F99" : "#004445"
        border.width: 0
        radius: 10
    }
}


/*##^##
Designer {
    D{i:0;formeditorZoom:1.75}
}
##^##*/
