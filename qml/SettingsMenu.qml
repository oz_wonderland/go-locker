import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.15
import QtQuick.Dialogs 1.3
import QtGraphicalEffects 1.0

Window{
    id: settings_menu
    width: 1280
    height: 720
    visible: true
    color: "#6ab688"
    Loader { id: ad_screen }
    StackView {
        id: stack_view
        anchors.fill: parent
        initialItem: settings_page
    }
    PopUpMessage {
            id: popup_remove_admin
            message: "Voulez-vous vraiment supprimer ADMIN?"
            button_text_left_popup: "NON"
            button_text_rigth_popup: "OUI"
    }
    PopUpMessage {
            id: popup_remove_asv
            message: "Voulez-vous vraiment supprimer ASV?"
            button_text_left_popup: "NON"
            button_text_rigth_popup: "OUI"
    }
    //Confirmed new admin pop up
    PopUpIcon{
        id: popup_icon_new_admin
        message: "ADMIN créé avec succès!"
        iconPath: "drawables/check_circle_fill.svg"
    }
    //Confirmed edit admin pop up
    PopUpIcon{
        id: popup_icon_edit_admin
        message: "ADMIN modification réussie!"
        iconPath: "drawables/check_circle_fill.svg"
    }
    //Confirmed delete admin pop up
    PopUpIcon{
        id: popup_icon_delete_admin
        message: "ADMIN supprimé avec succès!"
        iconPath: "drawables/check_circle_fill.svg"
    }
    //Confirmed new asv pop up
    PopUpIcon{
        id: popup_icon_new_asv
        message: "ASV créé avec succès!"
        iconPath: "drawables/check_circle_fill.svg"
    }
    //Confirmed edit asv pop up
    PopUpIcon{
        id: popup_icon_edit_asv
        message: "ASV modification réussie!"
        iconPath: "drawables/check_circle_fill.svg"
    }
    //Confirmed delete asv pop up
    PopUpIcon{
        id: popup_icon_delete_asv
        message: "ASV supprimé avec succès!"
        iconPath: "drawables/check_circle_fill.svg"
    }
    //SETTINGS PAGE
    Item{
        id: settings_page        
        visible: true
        //Top bar
        TopBar{
            id: top_bar_settings
            barText: "Définitions"
            iconCirclePath: "drawables/2_gears.svg"
        }
        //The "question"
        Text {
            id: question
            text: qsTr("Que souhaitez-vous faire?")
            font.weight: Font.Bold
            font.pointSize: 24
            font.family: "Open Sans"
            anchors.bottom: two_buttons.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: 40
            color: "#ffffff"
        }
        //Two text buttons in the middle
        RowLayout{
            id: two_buttons
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            TextButton{
                id: button_left
                Layout.row: 2
                Layout.column: 1
                width_button: 345
                height_button: 140
                Layout.preferredWidth: 345
                Layout.preferredHeight: 140
                buttonText: "Gestion des paramètres"
                Layout.alignment: Qt.AlignLeft
                Layout.rightMargin: 20                
            }
            TextButton{
                id: button_right
                Layout.row: 2
                Layout.column: 1
                width_button: 345
                height_button: 140
                Layout.preferredWidth: 345
                Layout.preferredHeight: 140
                buttonText: "Gestion des profils"
                onClicked: {                    
                    stack_view.push(profile_management)
                }
            }
         }
        //Leave button
        IconButton {
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 42
            anchors.rightMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/box_arrow_left.svg"
            buttonText: "Sortir"
            onClicked: ad_screen.source="AdScreen.qml"
         }
    }
    //PROFILE MANAGEMENT
    Item{
        id: profile_management        
        visible:false
        //Top bar
        TopBar{
            //widthBar: 1234
            id: top_bar_profile_management
            barText: "Gestion des profils"
            iconCirclePath: "drawables/2_gears.svg"
        }
        //Two text buttons in the middle
        RowLayout{
            id: two_buttons_profile_management
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            TextButton{
                id: button_left_profile_management
                Layout.row: 2
                Layout.column: 1
                width_button: 345
                height_button: 140
                Layout.preferredWidth: 345
                Layout.preferredHeight: 140
                buttonText: "Administrateur"
                Layout.alignment: Qt.AlignLeft
                Layout.rightMargin: 20
                onClicked: {
                    stack_view.push(admin_management)
                }
            }
            TextButton{
                id: button_right_profile_management
                Layout.row: 2
                Layout.column: 1
                width_button: 345
                height_button: 140
                Layout.preferredWidth: 345
                Layout.preferredHeight: 140
                buttonText: "ASV"
                onClicked: {
                    stack_view.push(asv_management)
                }
            }
         }
        //Return button
        IconButton {
            visible: true
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.leftMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/arrow_return_left.svg"
            buttonText: "Retour"
            onClicked: {                
                stack_view.pop()
            }
         }
        //Leave button
        IconButton {
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 42
            anchors.rightMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/box_arrow_left.svg"
            buttonText: "Sortir"
            onClicked: ad_screen.source="AdScreen.qml"
         }
    }
    //COMPONENT TO POPULATE LISTS
    Component.onCompleted: {
        for (var i = 0; i <5; i++) {
            //--> populate admin list
            admin_profile_list.model.append({title_value: "ADMIN" + (i+1) , text_value: "Nom ADMIN"});
            //--> populate asv list
            asv_profile_list.model.append({title_value: "ASV" + (i+1) , text_value: "Nom ASV"});
        }
    }
    //ADMIN MANAGEMENT
    Item {
        id: admin_management
        visible:false
        //Top bar
        TopBar{
            id: top_bar_admin_management
            barText: "Gestion des profils"
            iconCirclePath: "drawables/2_gears.svg"
        }
        //Sub bar
        SubBar{
            id: sub_bar_admin_management
            barText: "ADMIN'S"
            anchors.top: top_bar_admin_management.bottom
        }
        //Icon Buttons bellow the subbar
        //New admin
        IconButton{
            id: new_admin_button
            anchors.top: sub_bar_admin_management.bottom
            anchors.topMargin: 38
            anchors.left: parent.left
            anchors.leftMargin: 23
            width_button: 448
            height_button: 56
            iconSource: "drawables/person_plus_fill.svg"
            buttonText: "Ajouter nouveau"
            onClicked: {
                stack_view.push(new_admin)
            }
        }
        //Edit admin
        IconButton{
            id: edit_admin_button
            anchors.top: new_admin_button.top
            anchors.right: delete_admin_button.left
            anchors.rightMargin: 23
            width_button: 247
            height_button: 56
            iconSource: "drawables/pencil.svg"
            buttonText: "Modifier"
            onClicked: {
                stack_view.push(edit_admin)
            }
        }
        //Delete admin
        IconButton{
            id: delete_admin_button
            anchors.top: new_admin_button.top
            anchors.right: parent.right
            anchors.rightMargin: 23
            width_button: 247
            height_button: 56
            standardColorUp: "#D86363"
            standardColorDown: "#FF5F5F"
            iconSource: "drawables/delete.svg"
            buttonText: "Supprimer"
            onClicked: {                
                popup_remove_admin.open()
            }
        }
        //List for profiles
        ProfileList{
            id:admin_profile_list
            title_value: title_value
            text_value: text_value
            anchors.top: new_admin_button.bottom
            anchors.topMargin: 38

        }
        //Return button
        IconButton {
            visible: true
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.leftMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/arrow_return_left.svg"
            buttonText: "Retour"
            onClicked: {
                stack_view.pop()
            }
         }
        IconButton {
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 42
            anchors.rightMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/box_arrow_left.svg"
            buttonText: "Sortir"
            onClicked: ad_screen.source="AdScreen.qml"
         }
    }
    //NEW ADMIN
    Item {
        id: new_admin
        visible: false
        //Top bar
        TopBar{
            id: top_bar_new_admin
            barText: "Gestion des profils"
            iconCirclePath: "drawables/2_gears.svg"
        }
        //Sub bar
        SubBar{
            id: sub_bar_new_admin
            barText: "Nouveau profil ADMIN"
            anchors.top: top_bar_new_admin.bottom
        }
        //Name textfield
        CustomTextField{
            id: new_admin_name
            anchors.top: sub_bar_new_admin.bottom
            topMarginValue: 20
            textField: "Nom"
        }
        //Surname textfield
        CustomTextField{
            id: new_admin_surname
            anchors.top: new_admin_name.bottom
            topMarginValue: 9
            textField: "Prénom"
        }
        //Code text field
        CustomTextField{
            id: new_admin_code
            anchors.top: new_admin_surname.bottom
            topMarginValue: 9
            textField: "CODE ADMIN"
            textInput: "Code créé antomatiquement"
        }
        //Return button
        IconButton {
            visible: true
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.leftMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/arrow_return_left.svg"
            buttonText: "Retour"
            onClicked: {
                stack_view.pop()
            }
         }
        //Validate button
        IconButton {
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.rightMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/check_circle_fill.svg"
            buttonText: "Valider"
            onClicked: {
                popup_icon_new_admin.open()
            }
         }
    }
    //EDIT ADMIN
    Item {
        id: edit_admin
        visible: false
        //Top bar
        TopBar{
            id: top_bar_edit_admin
            barText: "Gestion des profils"
            iconCirclePath: "drawables/2_gears.svg"
        }
        //Sub bar
        SubBar{
            id: sub_bar_edit_admin
            barText: "Modification du profil ADMIN"
            anchors.top: top_bar_edit_admin.bottom
        }
        //Name textfield
        CustomTextField{
            id: edit_admin_name
            anchors.top: sub_bar_edit_admin.bottom
            topMarginValue: 20
            textField: "Nom"
            textInput: "ADMIN X"
        }
        //Surname textfield
        CustomTextField{
            id: edit_admin_surname
            anchors.top: edit_admin_name.bottom
            topMarginValue: 9
            textField: "Prénom"
            textInput: "ADMIN prénom X"
        }
        //Code text field
        CustomTextField{
            id: edit_admin_code
            anchors.top: edit_admin_surname.bottom
            topMarginValue: 9
            textField: "CODE ADMIN"
            textInput: "SD3FGR"
        }
        //Return button
        IconButton {
            visible: true
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.leftMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/arrow_return_left.svg"
            buttonText: "Retour"
            onClicked: {
                stack_view.pop()
            }
         }
        //Validate button
        IconButton {
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.rightMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/check_circle_fill.svg"
            buttonText: "Valider"
            onClicked: {
                popup_icon_edit_admin.open()
            }
         }
    }
    //ASV MANAGEMENT
    Item {
        id: asv_management
        visible:false
        //Top bar
        TopBar{
            id: top_bar_asv_management
            barText: "Gestion des profils"
            iconCirclePath: "drawables/2_gears.svg"
        }
        //Sub bar
        SubBar{
            id: sub_bar_asv_management
            barText: "ASV'S"
            anchors.top: top_bar_asv_management.bottom
        }
        //Icon Buttons bellow the subbar
        //New asv
        IconButton{
            id: new_asv_button
            anchors.top: sub_bar_asv_management.bottom
            anchors.topMargin: 38
            anchors.left: parent.left
            anchors.leftMargin: 23
            width_button: 448
            height_button: 56
            iconSource: "drawables/person_plus_fill.svg"
            buttonText: "Ajouter nouveau"            
            onClicked: {
                stack_view.push(new_asv)
            }
        }
        //Edit asv
        IconButton{
            id: edit_asv_button
            anchors.top: new_asv_button.top
            anchors.right: delete_asv_button.left
            anchors.rightMargin: 23
            width_button: 247
            height_button: 56
            iconSource: "drawables/pencil.svg"
            buttonText: "Modifier"
            onClicked: {
                stack_view.push(edit_asv)
            }
        }
        //Delete asv
        IconButton{
            id: delete_asv_button
            anchors.top: new_asv_button.top
            anchors.right: parent.right
            anchors.rightMargin: 23
            width_button: 247
            height_button: 56
            standardColorUp: "#D86363"
            standardColorDown: "#FF5F5F"
            iconSource: "drawables/delete.svg"
            buttonText: "Supprimer"
            onClicked: {
                popup_remove_asv.open()
            }
        }
        //List for asv
        ProfileList{
            id: asv_profile_list
            title_value: title_value
            text_value: text_value
            anchors.top: new_asv_button.bottom
            anchors.topMargin: 38
        }
        //Return button
        IconButton {
            visible: true
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.leftMargin: 23
            width_button: 170
            height_button: 70             
            iconSource: "drawables/arrow_return_left.svg"
            buttonText: "Retour"
            onClicked: {
                stack_view.pop()
            }
         }
        //Leave button
        IconButton {
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 42
            anchors.rightMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/box_arrow_left.svg"
            buttonText: "Sortir"
            onClicked: ad_screen.source="AdScreen.qml"
         }

    }
    //NEW ASV
    Item {
        id: new_asv
        visible: false
        //Top bar
        TopBar{
            id: topbar_new_asv
            barText: "Gestion des profils"
            iconCirclePath: "drawables/2_gears.svg"
        }
        //Sub bar
        SubBar{
            id: sub_bar_new_asv
            barText: "Nouveau profil ASV"
            anchors.top: topbar_new_asv.bottom
        }
        //Name textfield
        CustomTextField{
            id: new_asv_name
            anchors.top: sub_bar_new_asv.bottom
            topMarginValue: 20
            textField: "Nom"
        }
        //Surname textfield
        CustomTextField{
            id: new_asv_surname
            anchors.top: new_asv_name.bottom
            topMarginValue: 9
            textField: "Prénom"
        }
        //Code text field
        CustomTextField{
            id: new_asv_code
            anchors.top: new_asv_surname.bottom
            topMarginValue: 9
            textField: "CODE ASV"
            textInput: "Code créé antomatiquement"
        }
        //Options rectangle
        Rectangle{
            id: option_bar_new
            width: 742
            height: 44
            radius: 10
            clip: false
            color: "#DDFFEC"
            anchors.top: new_asv_code.bottom
            anchors.leftMargin: -18
            anchors.left: parent.left
            anchors.topMargin: 9
            Text {
                id: text_option_new
                text:  "Veuillez cocher les options associées à cette ASV"
                font.pointSize: 20
                font.family: "Open Sans"
                color: "#000000"
                anchors.verticalCenter: option_bar_new.verticalCenter
                anchors.left: parent.left
                font.letterSpacing: -0.2
                clip: false
                anchors.leftMargin: 41
            }
        }
        //Check boxes
        CustomCheckBox{
            id: check_box_new_asv_left
            anchors.top: option_bar_new.bottom
            anchors.topMargin: 9
            checkBoxText: "Approvisionner"
        }
        CustomCheckBox{
            id: check_box_new_asv_right
            anchors.top: option_bar_new.bottom
            anchors.topMargin: 9
            anchors.left: check_box_new_asv_left.right
            checkBoxText: "Gérer les retours"
        }
        //Return button
        IconButton {
            visible: true
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.leftMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/arrow_return_left.svg"
            buttonText: "Retour"
            onClicked: {
                stack_view.pop()
            }
         }
        //Validate button
        IconButton {
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.rightMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/check_circle_fill.svg"
            buttonText: "Valider"
            onClicked: {
                popup_icon_new_asv.open()
            }
         }
    }
    //EDIT ASV
    Item {
        id: edit_asv
        visible: false
        //Top bar
        TopBar{
            id: top_bar_edit_asv
            barText: "Gestion des profils"
            iconCirclePath: "drawables/2_gears.svg"
        }
        //Sub bar
        SubBar{
            id: sub_bar_edit_asv
            barText: "Modification du profil ASV"
            anchors.top: top_bar_edit_asv.bottom
        }
        //Name textfield
        CustomTextField{
            id: edit_asv_name
            anchors.top: sub_bar_edit_asv.bottom
            topMarginValue: 20
            textField: "Nom"
        }
        //Surname textfield
        CustomTextField{
            id: edit_asv_surname
            anchors.top: edit_asv_name.bottom
            topMarginValue: 9
            textField: "Prénom"
        }
        //Code text field
        CustomTextField{
            id: edit_asv_code
            anchors.top: edit_asv_surname.bottom
            topMarginValue: 9
            textField: "CODE ASV"
            textInput: "Code créé antomatiquement"
        }
        //Options rectangle
        Rectangle{
            id: option_bar_edit
            width: 742
            height: 44
            radius: 10
            clip: false
            color: "#DDFFEC"
            anchors.top: edit_asv_code.bottom
            anchors.leftMargin: -18
            anchors.left: parent.left
            anchors.topMargin: 9
            Text {
                id: text_option_edit
                text:  "Veuillez cocher les options associées à cette ASV"
                font.pointSize: 20
                font.family: "Open Sans"
                color: "#000000"
                anchors.verticalCenter: option_bar_edit.verticalCenter
                anchors.left: parent.left
                font.letterSpacing: -0.2
                clip: false
                anchors.leftMargin: 41
            }
        }
        //Check boxes
        CustomCheckBox{
            id: check_box_edit_asv_left
            anchors.top: option_bar_edit.bottom
            anchors.topMargin: 9
            checkBoxText: "Approvisionner"
        }
        CustomCheckBox{
            id: check_box_edit_asv_right
            anchors.top: option_bar_edit.bottom
            anchors.topMargin: 9
            anchors.left: check_box_edit_asv_left.right
            checkBoxText: "Gérer les retours"
        }
        //Return button
        IconButton {
            visible: true
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.leftMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/arrow_return_left.svg"
            buttonText: "Retour"
            onClicked: {
                stack_view.pop()
            }
         }
        //Validate button
        IconButton {
            visible: true
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 35
            anchors.rightMargin: 23
            width_button: 170
            height_button: 70
            iconSource: "drawables/check_circle_fill.svg"
            buttonText: "Valider"
            onClicked: {
                popup_icon_edit_asv.open()
            }
        }
    }
 }
