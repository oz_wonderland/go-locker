import QtQuick 2.0

Rectangle{
    property int widthBar
    property string barText

    id: sub_bar
    width: widthBar
    height: 57
    radius: 20
    clip: false
    color: "#3E9F99"
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: 23
    anchors.rightMargin: 23
    anchors.topMargin: 10
    Text {
        id: bar_text
        text:  barText
        font.weight: Font.Bold
        font.pointSize: 36
        font.family: "Open Sans"
        color: "#ffffff"
        anchors.verticalCenter: sub_bar.verticalCenter
        anchors.left: sub_bar.left
        anchors.leftMargin: 25
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

    }

}
