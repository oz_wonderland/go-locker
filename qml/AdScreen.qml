import QtQuick 2.0
import QtQuick.Window 2.13
import QtQuick.Controls 2.13
import QtMultimedia 5.15

Window {
    id: ad_screen_window
    width: 1280
    height: 720
    minimumHeight: height
    minimumWidth: width
    maximumHeight: minimumHeight
    maximumWidth: minimumWidth
    visible: true

    SwipeView {
        id: ad_screen_swipe_view
        currentIndex: 0
        anchors.fill: parent

        Item {
            id: firstPage
            Image {
                id: ad1
                source: "drawables/ad1.png"
                anchors.fill: parent
            }
            Loader { id: pageLoader1 }
            MouseArea {
                anchors.fill: parent
                onClicked: pageLoader1.source = "main.qml"
            }
        }
        Item {
            id: secondPage
            Image {
                id: ad2
                source: "drawables/ad2.png"
                anchors.fill: parent
            }
            Loader { id: pageLoader2 }
            MouseArea {
                anchors.fill: parent
                onClicked: pageLoader2.source = "main.qml"
            }
        }
    }

    Timer {
        running: true
        repeat: true
        interval: 4000
        onTriggered: {
          var nextIndex = (ad_screen_swipe_view.currentIndex + 1) % ad_screen_swipe_view.count
          ad_screen_swipe_view.setCurrentIndex(nextIndex)
        }
      }

}
