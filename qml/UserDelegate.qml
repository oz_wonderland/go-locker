import QtQuick 2.13
import QtQuick.Layouts 1.11

Component {
    id: userDelegate
    Item {
        width: 720
        height: 75

        RowLayout {
            anchors.fill: parent
            Image {
                id: image
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
                source: "drawables/face-black-48dp.svg"
                Layout.maximumHeight: 50
                Layout.maximumWidth: 50
                Layout.minimumHeight: 50
                Layout.minimumWidth: 50
                Layout.preferredHeight: 50
                Layout.preferredWidth: 50
                fillMode: Image.PreserveAspectFit
            }
            ColumnLayout{
                x: 0
                y: 0
                width: 0
                height: 0
                clip: false
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.fillWidth: true
                Layout.fillHeight: true
                Text { text: '<b>' + firstName + ' ' + lastName + '</b>' }
                Text { text: '<b>téléphone:</b> ' + phone }
                Text { text: '<b>email:</b> ' + email }
            }
        }
    }
}
