import QtQuick 2.13
import QtQuick.Controls 2.13

Page {
    id: auth_view_page

    Banner {
        id: auth_view_banner
        height: Styles.bannerHeight
        title: "Choisissez le mode"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
    }
    AuthContent {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: auth_view_banner.bottom
    }
}
