import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls 2.13

Window {
    id: window
    width: 1280
    height: 800

    visible: true

    FontLoader { id: fixedFont; name: "Fredoka One" }

    Loader { id: pageLoader }

    Image {
        id: vetline_logo_image
        anchors.left: parent.left
        anchors.top: parent.top
        source: "drawables/logo_vetline.svg"
        anchors.leftMargin: 24
        anchors.topMargin: 24
    }

    Text {
        id: welcome_text
        text: "Bienvenue à"
        anchors.top: parent.top
        anchors.bottom: vet_go_logo_image.top
        horizontalAlignment: Text.AlignHCenter
        anchors.topMargin: 237
        anchors.bottomMargin: 24
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: "Fredoka One"
        font.pointSize: 48
    }

    Text {
        id: click_to_continue_text
        text: "Pour continuer, cliquez sur l'écran"
        anchors.top: vet_go_logo_image.bottom
        horizontalAlignment: Text.AlignHCenter
        anchors.topMargin: 100
        anchors.horizontalCenter: parent.horizontalCenter
        font.family: "Open Sans"
        font.pointSize: 24
    }

    Image {
        id: vet_go_logo_image
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: welcome_text.top
        anchors.bottom: click_to_continue_text.bottom
        source: "drawables/vet_go_logo.svg"
        anchors.rightMargin: 330
        anchors.leftMargin: 360
        anchors.bottomMargin: 100
        anchors.topMargin: 84
    }

    Button{
        anchors.fill: parent
        opacity: 0
        onClicked: {
            pageLoader.source = ""
            pageLoader.source = "main.qml"
        }
    }
}
