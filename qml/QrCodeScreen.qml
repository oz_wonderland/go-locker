import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.11
import QtMultimedia 5.15
import QZXing 2.3

Window {
    id: qrCodeScreen
    width: 1280
    height: 720
    visible: true

    Camera {
            id: camera

            imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

            exposure {
                exposureCompensation: -1.0
                exposureMode: Camera.ExposurePortrait
            }

            flash.mode: Camera.FlashRedEyeReduction
        }

    QZXingFilter{
        id:zxingFilter

        decoder{
            enabledDecoders: QZXing.DecoderFormat_QR_CODE | QZXing.DecoderFormat_EAN_13
        }
    }

    Banner {
        id: banner
        height: 150
        title: "Scannez votre QR code"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0

        BackButton {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.rightMargin: 35
            anchors.bottomMargin: 40
            anchors.topMargin: 53
            onClicked: qrCodeScreen.close() | camera.stop()
        }
    }

    Rectangle{
        color: Colors.backgroundColor
        id: qr_code_row_rectangle
        anchors.top: banner.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.bottomMargin: 0
        anchors.topMargin: 0

        RowLayout{
            id: rawLayout_qrcode
            anchors.fill: parent
            spacing: 10
            VideoOutput{
                id: videooutput_qrcode
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 640
                Layout.margins: 40
                width: 640
                source: camera
                filters: [ zxingFilter ]
            }

            AnimatedImage{
                id: arrow_rigth_gif
                Layout.fillHeight: true
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 640
                width: 640
                source: "drawables/arrow_rigth.gif"
                Layout.margins: 40
            }
        }
    }

    Timer{
        interval: 8000
        running: true
        repeat: true
        onTriggered: pop_up_message.open()
    }

    PopUpMessage{
        id: pop_up_message
        message: "Il n'a pas été possible de voir le code QR.\nSouhaitez-vous le saisir manuellement?"
        button_text_left_popup: "NON"
        button_text_rigth_popup: "OUI"
        onOpened: camera.stop()
        onClosed: camera.start()
    }
}



