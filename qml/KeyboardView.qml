import QtQuick 2.13
import QtQuick.Controls 2.13

Page {
    id: keyboard_view_page

    Banner {
        id: keyboard_view_banner
        height: Styles.bannerHeight
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        title: "Veuillez introduire votre code"

        BackButton {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.rightMargin: 35
            anchors.bottomMargin: 40
            anchors.topMargin: 53
        }
    }
    KeyboardContent {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: keyboard_view_banner.bottom
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:800;width:1280}
}
##^##*/
