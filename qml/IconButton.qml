import QtQuick 2.13
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.11


RoundButton{
    property int width_button
    property int height_button
    property string iconSource
    property string buttonText
    property string standardColorUp: "#004445"
    property string standardColorDown: "#3E9F99"

    id: icon_button
    text:  qsTr("Retour")
    font.weight: Font.Bold
    font.pointSize: 20
    font.family: "Open Sans"

    Image {
        antialiasing: true
        id: image_icon_button
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.alignWhenCentered: Text
        source: iconSource
        anchors.leftMargin: 7
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        fillMode: Image.PreserveAspectFit
    }

    contentItem: Text {
            font: icon_button.font
            color: "#ffffff"
            text: buttonText
            anchors.rightMargin: 7
            anchors.leftMargin: 3
            elide: Text.ElideRight
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: image_icon_button.right
            anchors.right: parent.right
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
    }

    background: Rectangle {
        implicitWidth: width_button
        implicitHeight: height_button
        opacity: enabled ? 1 : 0.3
        color: icon_button.down ? standardColorDown : standardColorUp
        border.width: 0
        radius: 10
    }
}
