import QtQuick 2.13

Rectangle {
    property string title

    id: banner_rectangle
    color: Styles.bannerBackgroundColor

    Image {
        id: logo_image_bar
        y: 0
        width: 224
        height: 150
        anchors.verticalCenter: parent.verticalCenter

        anchors.left: parent.left
        source: "drawables/vetgoLogo.png"
        fillMode: Image.PreserveAspectFit
        anchors.rightMargin: 15
    }

    Text {
        id: banner_title_text
        color: "white"
        text: title
        anchors.left: logo_image_bar.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        font.pixelSize: 45
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        maximumLineCount: 1
        fontSizeMode: Text.Fit
        font.bold: true
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:150;width:1280}D{i:1}
}
##^##*/
