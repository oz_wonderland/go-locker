import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.11


    ListView{
        property string title_value
        property  string text_value

        id:profile_list_view        
        anchors.left: parent.left
        anchors.leftMargin: 23
        anchors.right: parent.right
        height: 234
        model: ListModel{}
        spacing: 10
        orientation: ListView.Horizontal
        delegate: RoundButton{
            id: profile_button
            Text {
                id: profile_button_title
                text: title_value
                font.weight: Font.Bold
                font.pointSize: 28
                font.family: "Open Sans"
                color: "#ffffff"
                anchors.top: parent.top
                anchors.topMargin: 23
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Text {
                id: profile_button_text
                text: text_value
                font.weight: Font.Bold
                font.pointSize: 28
                font.family: "Open Sans"
                color: "#ffffff"
                anchors.top: profile_button_title.bottom
                anchors.topMargin: parent.width/3
                anchors.horizontalCenter: parent.horizontalCenter
            }

            background: Rectangle {
                id: background
                implicitWidth: 284
                implicitHeight: 234
                opacity: enabled ? 1 : 0.3
                color: profile_button.down ? "#3E9F99" : "#004445"
                border.width: 0
                radius: 10
            }
        }
    }


