import QtQuick 2.0
import QtQuick.Layouts 1.11

Rectangle {
    id: settings_content_rectangle
    color: Styles.backgroundColor

    RowLayout{
        anchors.fill: parent
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        TextButton{
            id: settings_content_params_button
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.row: 2
            Layout.column: 1
            width_button: 345
            height_button: 140
            Layout.preferredWidth: 345
            Layout.preferredHeight: 140
            buttonText: "Gestion des paramètres"
            Layout.rightMargin: 20
        }
        TextButton{
            id: settings_content_users_button
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.row: 2
            Layout.column: 1
            width_button: 345
            height_button: 140
            Layout.preferredWidth: 345
            Layout.preferredHeight: 140
            buttonText: "Gestion des profils"
            onClicked: main_window.onOpenProfileManagementView()
        }
     }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:650;width:1280}
}
##^##*/
