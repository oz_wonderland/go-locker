import QtQuick 2.13
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.15

Rectangle{
    property string checkBoxText
    property bool   state: false

    id: custom_check_box
    height: 55
    width: 340
    radius: 20
    color: "#3E9F99"
    anchors.left: parent.left
    anchors.leftMargin: 23
    CheckBox {
        id: check_box
        checked: state
        anchors.verticalCenter: custom_check_box.verticalCenter
        anchors.left: custom_check_box.left
        anchors.leftMargin: 25
        indicator: Rectangle {
            id: custom_box
            color: "#6ab688"
            implicitWidth: 33
            implicitHeight: 33
            radius: 10
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
        }
        contentItem:Text {
            id:check_box_text
            text:  checkBoxText
            font.weight: Font.Bold
            font.pointSize: 20
            font.family: "Open Sans"
            color: "#ffffff"
            font.letterSpacing: -0.2
            anchors.verticalCenter: custom_box.verticalCenter
            anchors.left: custom_box.right
            anchors.leftMargin: 15
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        Image{
            visible: check_box.checked
            anchors.verticalCenter: parent.verticalCenter
            anchors.leftMargin: 4
            source: "drawables/check2.svg"

        }
    }
}

