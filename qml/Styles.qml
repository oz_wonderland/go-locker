//Style.qml with custom singleton type definition
pragma Singleton
import QtQuick 2.0

QtObject {
    property color backgroundColor: "#6ab688"
    property color bannerBackgroundColor: "#031719"
    property int width: 1280
    property int height: 800
    property int bannerHeight: 150
}
