import QtQuick 2.2
import QtQuick.Controls 1.2

Rectangle{
    property string textField
    property string textInput
    property int topMarginValue

    id: rectangle
    height: 85
    radius: 20
    clip: false
    color: "#3E9F99"
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: 23
    anchors.rightMargin: 23
    anchors.topMargin: topMarginValue
    Text {
        id: text_field
        text:  textField+":"
        font.weight: Font.Bold
        font.pointSize: 32
        font.family: "Open Sans"
        color: "#ffffff"
        anchors.verticalCenter: rectangle.verticalCenter
        anchors.left: rectangle.left
        anchors.leftMargin: 25
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
    TextInput{
        color: "#ffffff"
        text: textInput
        font.pointSize: 30
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: text_field.right
        anchors.right: parent.right
        font.italic: true
        anchors.leftMargin: 5
    }

}
