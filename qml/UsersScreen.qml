import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Layouts 1.11

Window {
    id: window
    width: 1280
    height: 720
    visible: true
    color: Colors.backgroundColor

    Banner {
        id: banner
        title: "Utilisateurs"
        height: 150
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
    }

    Component {
        id: userDelegate
        Item {
            width: 720
            height: 75

            RowLayout {
                anchors.fill: parent
                Image {
                    id: image
                    horizontalAlignment: Image.AlignHCenter
                    verticalAlignment: Image.AlignVCenter
                    source: "drawables/face-black-48dp.svg"
                    fillMode: Image.PreserveAspectFit
                    Layout.maximumHeight: 50
                    Layout.maximumWidth: 50
                    Layout.minimumHeight: 50
                    Layout.minimumWidth: 50
                    Layout.preferredHeight: 50
                    Layout.preferredWidth: 50
                }
                ColumnLayout{
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Text { text: '<b>' + firstname + ' ' + lastname + '</b>' }
                    Text { text: '<b>téléphone:</b> ' + phone }
                    Text { text: '<b>email:</b> ' + email }
                }
            }
        }
    }


    Rectangle{
        color: "white"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: banner.bottom
        anchors.bottom: parent.bottom
        anchors.topMargin: 50
        anchors.rightMargin: 50
        anchors.leftMargin: 50
        anchors.bottomMargin: 50
        ListView {
            id: listView
            anchors.fill: parent
            anchors.topMargin: 50
            anchors.rightMargin: 50
            anchors.leftMargin: 50
            anchors.bottomMargin: 50
            model: userModel
            delegate: userDelegate
            focus: true
        }
    }
}
