import QtQuick 2.0
import QtQuick.Layouts 1.11

RowLayout{
        property string buttonLeftText
        property string buttonRightText

        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

        TextButton{
            id: button_left
            Layout.row: 2
            Layout.column: 1
            width_button: 345
            height_button: 140
            Layout.preferredWidth: 345
            Layout.preferredHeight: 140
            buttonText: buttonLeftText
            anchors.right: button_right.left
            anchors.rightMargin: 20
        }
        TextButton{
            id: button_right
            Layout.row: 2
            Layout.column: 1
            width_button: 345
            height_button: 140
            Layout.preferredWidth: 345
            Layout.preferredHeight: 140
            buttonText: buttonRightText
        }
}
