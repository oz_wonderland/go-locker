import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Controls 2.13

Window {
    id: main_window
    width: Styles.width
    height: Styles.height
    visible: true
    color: Styles.backgroundColor
    signal backClicked()
    signal showKeyboardView()
    signal onOpenSettingsView()
    signal onOpenProfileManagementView()
    signal onOpenUserManagementView()
    signal showQrCodeView()

    StackView {
        id: main_stack_view
        initialItem: main_component
        anchors.fill: parent
        focusPolicy: Qt.TabFocus
        onCurrentItemChanged: {
            currentItem.forceActiveFocus()
        }
    }

    Component {
        id: main_component
        AuthView{}
    }

    Component.onCompleted: {
        main_window.backClicked.connect(popBack)
        main_window.showKeyboardView.connect(openKeyboardView)
        main_window.onOpenSettingsView.connect(openSettingsView)
        main_window.onOpenProfileManagementView.connect(openProfileManagementView)
        main_window.onOpenUserManagementView.connect(openUserManagementView)
    }

    function popBack() {
        main_stack_view.pop()
    }

    function backHome() {
        main_stack_view.push(null)
    }

    function openKeyboardView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/KeyboardView.qml"))
    }

    function openQrCodeView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/QrCodeScreen.qml"))
    }

    function openSettingsView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/SettingsView.qml"))
    }

    function openProfileManagementView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/ProfileManagementView.qml"))
    }

    function openUserManagementView() {
        main_stack_view.push(Qt.resolvedUrl("qrc:/qml/UserManagementView.qml"))
    }
}
