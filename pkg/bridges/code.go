package bridges

import (
	"context"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/qml"
	"go.uber.org/zap"

	"gitlab.com/oz_wonderland/locker/pkg/storage/psql"
)

type Code struct {
	core.QObject

	_ func(code string) string `slot:"codeInput"`
}

type codeSvc struct {
	store  *psql.Store
	logger *zap.Logger
}

func initCode(engine *qml.QQmlApplicationEngine, logger *zap.Logger, store *psql.Store) {
	code := NewCode(nil)
	codeSvc := codeSvc{logger: logger, store: store}
	code.ConnectCodeInput(codeSvc.codeInput)

	engine.RootContext().SetContextProperty("codeBridge", code)
}

func (c *codeSvc) codeInput(code string) string {
	logger := c.logger.With(zap.String("action", "codeInput"))
	ctx := context.TODO()

	user, err := c.store.FindOneUserByCode(ctx, code)
	if err != nil {
		logger.With(zap.Error(err)).Error("error while searching for user")
		return ""
	}
	logger.With(zap.Reflect("user", user)).Info("found one user")
	return user.Type.String()
}
