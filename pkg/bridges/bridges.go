package bridges

import (
	"github.com/therecipe/qt/qml"
	"go.uber.org/zap"

	"gitlab.com/oz_wonderland/locker/pkg/storage/psql"
)

// Init creates and sets all application bridges
func Init(engine *qml.QQmlApplicationEngine, logger *zap.Logger, store *psql.Store) {
	initCode(engine, logger, store)
}
