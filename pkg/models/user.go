package models

import "time"

type Type byte
type Sex byte
type Status byte

const (
	UNKNOW_TYPE Type = iota
	ADMIN
	PARTNER
	CLIENT
)

const (
	UNKNOW_SEX Sex = iota
	WOMAN
	MAN
	OTHER
)

const (
	UNKNOW_STATUS Status = iota
	INACTIVE
	ACTIVE
	DELETED
	SUSPECT
)

var (
	_mapTypeToString = map[Type]string{
		UNKNOW_TYPE: "unknow",
		ADMIN:       "admin",
		PARTNER:     "asv",
		CLIENT:      "client",
	}
	_mapSexToString = map[Sex]string{
		UNKNOW_SEX: "unknow",
		WOMAN:      "woman",
		MAN:        "man",
		OTHER:      "other",
	}
	_mapStatusToString = map[Status]string{
		UNKNOW_STATUS: "unknow",
		INACTIVE:      "inactive",
		ACTIVE:        "active",
		DELETED:       "deleted",
		SUSPECT:       "suspect",
	}
)

type Users []*User

type User struct {
	CreatedAt     *time.Time
	LastUpdatedAt *time.Time
	Type          Type
	Sex           Sex
	Status        Status
	Lastname      string
	Firstname     string
	Email         string
	Phone         string
	CreatedBy     string
	LastUpdatedBy string
	Code          string
}

func NewUser() *User {
	return &User{}
}

func (t Type) String() string {
	return _mapTypeToString[t]
}

func (s Sex) String() string {
	return _mapSexToString[s]
}

func (s Status) String() string {
	return _mapStatusToString[s]
}
