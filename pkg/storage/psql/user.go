package psql

import (
	"context"

	"gitlab.com/oz_wonderland/locker/pkg/models"
)

func (s *Store) FindOneUserByCode(ctx context.Context, code string) (*models.User, error) {
	user := &models.User{}
	result := s.db.First(user, "code = ?", code)
	if result.Error != nil {
		return nil, result.Error
	}
	return user, nil
}
