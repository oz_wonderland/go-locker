package psql

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Store struct {
	db *gorm.DB
}

func NewStore(psqlInfo string) (*Store, error) {
	db, err := gorm.Open(postgres.Open(psqlInfo), &gorm.Config{})
	return &Store{db}, err
}
