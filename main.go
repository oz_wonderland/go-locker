package main

import (
	"fmt"
	"os"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
	"go.uber.org/zap"

	"gitlab.com/oz_wonderland/locker/pkg/bridges"
	"gitlab.com/oz_wonderland/locker/pkg/storage/psql"
)

const (
	_host     = "localhost"
	_user     = "locker"
	_password = "locker"
	_dbName   = "xlocker"
	_port     = 5432
)

func initApp() (*psql.Store, error) {
	psqlInfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=disable",
		_host, _user, _password, _dbName, _port)
	return psql.NewStore(psqlInfo)
}

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to create logger %+v", err)
		os.Exit(-1)
	}
	defer logger.Sync()

	if err := os.Setenv("QT_IM_MODULE", "qtvirtualkeyboard"); err != nil {
		logger.With(zap.Error(err)).Error("failed to set QT_IM_MODULE env variable")
		os.Exit(-1)
	}
	store, err := initApp()
	if err != nil {
		logger.With(zap.Error(err)).Error("failed to connect to db")
		os.Exit(-1)
	}

	logger.Info("connected to db")

	core.QCoreApplication_SetApplicationName("Locker")
	core.QCoreApplication_SetOrganizationName("openstyles")
	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)

	gui.NewQGuiApplication(len(os.Args), os.Args)
	engine := qml.NewQQmlApplicationEngine(nil)
	engine.Load(core.NewQUrl3("qrc:/qml/main.qml", 0))
	bridges.Init(engine, logger, store)
	gui.QGuiApplication_Exec()
}
